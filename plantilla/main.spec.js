require('jasmine-check').install();
let main = require('./main');

//======================================================================================
test('arrayEqual', () => {
    expect(main.arrayEqual([], [])).toBeTruthy();
    expect(main.arrayEqual([1], [1])).toBeTruthy();
    expect(main.arrayEqual(["a"], ["a"])).toBeTruthy();
    expect(main.arrayEqual([1, "a"], [1, "a"])).toBeTruthy();
    expect(main.arrayEqual([1], ["a"])).toBeFalsy();
    expect(main.arrayEqual(["a"], [1])).toBeFalsy();    
});

check.it('arrayEqual-prop', [gen.int, gen.int], (a, b) => {
   expect(main.arrayEqual([a,b,b,a], [a,b,b,a])).toBeTruthy();
   expect(main.arrayEqual([a+1,b,b,a], [a,b,b,a-1])).toBeFalsy();
})

check.it('arrayEqual-prop2', [gen.int, gen.int], (a, b) => {
    expect(main.arrayEqual([a, [b,b] ,a], [a, [b,b] ,a])).toBeTruthy();    
 })

//======================================================================================
test('lastIndexWithCompare', () => {
    expect(main.lastIndexWithCompare([], 1)).toBe(-1);
    expect(main.lastIndexWithCompare(["a", 1, 3], 1)).toBeGreaterThan(-1);
    expect(main.lastIndexWithCompare(["a", [1, 2], 3], [1, 2])).toBe(-1);
    expect(main.lastIndexWithCompare(["a", [1, 2], 3], [1, 2], main.arrayEqual))
        .toBeGreaterThan(-1);
});

//======================================================================================
test('arrayMember', () => {
    expect(main.arrayMember([], "a")).toBeFalsy();
    expect(main.arrayMember([], 1)).toBeFalsy();
    expect(main.arrayMember([], [1, 2, 3])).toBeFalsy();
    expect(main.arrayMember([1, 2, "a"], "a")).toBeTruthy();
    expect(main.arrayMember([1, "a", 2],"a")).toBeTruthy();
    expect(main.arrayMember([ [], [1, 2, 3], [1]], [1, 2, 3])).toBeTruthy();
});

check.it('arrayMember-prop', [gen.array(gen.int, {minSize: 1})], (arr) => {
    for(x of arr) {
        expect(main.arrayMember(arr, x)).toBeTruthy();
    }
    expect(main.arrayMember(arr, "aff")).toBeFalsy();
})

//======================================================================================
test('arrayFilter', () => {
    expect(main.arrayFilter([1,2,3], x => x % 2 == 0)).toEqual([2]);
    expect(main.arrayFilter([1,2,3], x => x % 2 != 0)).toEqual([1,3]);
});

//======================================================================================
check.it('arrayMax', [gen.array(gen.int)], (arr) => {
    let max = main.arrayMax(arr);
    for(x of arr) {
        expect(x).toBeLessThanOrEqual(max);
    }
});

//======================================================================================
check.it('arrayMin', [gen.array(gen.int)], (arr) => {
    let max = main.arrayMin(arr);
    for(x of arr) {
        expect(x).toBeGreaterThanOrEqual(max);
    }
});

//======================================================================================
test('arrayReverse', () => {
    expect(main.arrayReverse([])).toEqual([]);
    expect(main.arrayReverse([1,2,3])).toEqual([3,2,1]);
    expect(main.arrayReverse([1, [1,2], 3])).toEqual([3, [1,2], 1]);
});

check.it('arrayReverse-prop', [gen.array(gen.int)], (arr) => {
    expect(main.arrayReverse(arr)).toEqual(arr.reverse());
});

check.it('arrayReverse-idempotent', [gen.array(gen.int)], (arr) => {
    expect(main.arrayReverse(main.arrayReverse(arr))).toEqual(arr);
});

//======================================================================================
test('arrayTake', () => {
    expect(main.arrayTake([], 1)).toEqual([]);
    expect(main.arrayTake([], 0)).toEqual([]);
    expect(main.arrayTake([1,2,3], 0)).toEqual([]);
    expect(main.arrayTake([1,2,3], 1)).toEqual([1]);
});

check.it('arrayTake-prop', [gen.array(gen.int, {minSize: 1}), gen.sPosInt], (arr, k) => {
    let _k = Math.min(k, arr.length);
    expect(main.arrayTake(arr, _k)).toEqual(arr.slice(0, _k));
});

//======================================================================================
test('arrayDrop', () => {
    expect(main.arrayDrop([], 1)).toEqual([]);
    expect(main.arrayDrop([], 0)).toEqual([]);
    expect(main.arrayDrop([1,2,3], 1)).toEqual([2,3]);
    expect(main.arrayDrop([1,2,3], 0)).toEqual([1,2,3]);
})

check.it('arrayDrop-prop', [gen.array(gen.int, {minSize : 1}), gen.sPosInt], (arr, k) => {
    let _k = Math.min(k, arr.length);
    expect(main.arrayDrop(arr, _k)).toEqual(arr.slice(_k, arr.length))
});

//======================================================================================
test('arrayRange', () => {
    expect(main.arrayRange([], 2, 10)).toEqual([]);
    expect(main.arrayRange([1], 2, 10)).toEqual([]);
    expect(main.arrayRange([1, 2, 3], 2, 10)).toEqual([3]);
    expect(main.arrayRange([1, 2, 3, 4, 5], 1, 4)).toEqual([2,3,4]);
});

check.it('arrayRange-prop', [gen.array(gen.int, {minSize: 1}), gen.sPosInt, gen.sPosInt], (arr, k1, k2) => {
    let a = Math.min(Math.min(k1, k2), arr.length);
    let b = Math.min(Math.max(k1, k2), arr.length);    
    expect(main.arrayRange(arr, a, b)).toEqual(arr.slice(a,b));
})

//======================================================================================
test('arrayAppend', () => {
    expect(main.arrayAppend( [], [])).toEqual([]);
    expect(main.arrayAppend([1], [])).toEqual([1]);
    expect(main.arrayAppend( [], [1])).toEqual([1]);
    expect(main.arrayAppend( [1,2,3], [4,5,6])).toEqual([1,2,3,4,5,6]);
    expect(main.arrayAppend( [4,5,6], [1,2,3])).toEqual([4,5,6,1,2,3]);
});

check.it('arrayAppend-prop', [gen.array(gen.int), gen.array(gen.int)], (arr1, arr2) => {
    expect(main.arrayAppend(arr1,arr2)).toEqual(arr1.concat(arr2));
});

//======================================================================================
test('arrayMinus', () => {
    expect(main.arrayMinus([])).toBe(0);
    expect(main.arrayMinus([1,2,3])).toBe(-4);
    expect(main.arrayMinus([3,2,1])).toBe(0);
});

check.it('arrayMinus-prop', [gen.array(gen.int, {minSize: 1})], (arr) => {
    expect(main.arrayMinus(arr)).toEqual(arr.slice(1).reduce((v,x) => v-x, arr[0]));
});

//======================================================================================
test('arrayAdd', () => {
    expect(main.arrayAdd([])).toBe(0);
    expect(main.arrayAdd([1,2,3])).toBe(6);
    expect(main.arrayAdd([3,2,1])).toBe(6);
});

check.it('arrayAdd-prop', [gen.array(gen.int)], (arr) => {
    expect(main.arrayAdd(arr)).toEqual(arr.reduce((v,x) => v + x, 0));
})

//======================================================================================
test('arrayMult', () => {
    expect(main.arrayMult([])).toBe(1);
    expect(main.arrayMult([1,2,3])).toBe(6);
    expect(main.arrayMult([3,2,1])).toBe(6);
});

check.it('arrayMult-prop', [gen.array(gen.int)], (arr) => {
    expect(main.arrayMult(arr)).toEqual(arr.reduce((v,x) => v*x, 1));
})

//======================================================================================
test('arrayFold', () => {
    expect(main.arrayFold((v,x) => v+x, [1,2,3], 0)).toEqual(6);
    expect(main.arrayFold((v,x) => v+x, [1,2,3], 1)).toEqual(7);
    expect(main.arrayFold((v,x) => v-x, [1,2,3], 0)).toEqual(-6);
});

//======================================================================================
test('arraySquare', () => {
    expect(main.arraySquare([])).toEqual([]);
    expect(main.arraySquare([1])).toEqual([1]);
    expect(main.arraySquare([1, 2, 3])).toEqual([1, 4, 9]);
});

check.it('arraySquare-prop', [gen.array(gen.int)], (arr) => {
    expect(main.arraySquare(arr)).toEqual(arr.map(x => x*x));
})

//======================================================================================
test('arrayAddOne', () => {
    expect(main.arrayAddOne([])).toEqual([]);
    expect(main.arrayAddOne([1])).toEqual([2]);
    expect(main.arrayAddOne([1, 2, 3])).toEqual([2, 3, 4]);
});

check.it('arrayAddOne-prop', [gen.array(gen.int)], (arr) => {
    expect(main.arrayAddOne(arr)).toEqual(arr.map(x => x+1));
});

//======================================================================================
test('arrayMap', () => {
    expect(main.arrayMap([1,2,3], x => x)).toEqual([1,2,3]);
    expect(main.arrayMap([1,2,3], x => x+1)).toEqual([2,3,4]);
    expect(main.arrayMap([1,2,3], x => x*x)).toEqual([1,4,9]);
});

//======================================================================================
test('arrayFlatten', () => {
    expect(main.arrayFlatten([])).toEqual([]);
    expect(main.arrayFlatten([[]])).toEqual([]);
    expect(main.arrayFlatten([[], []])).toEqual([]);
    expect(main.arrayFlatten([[1], [2]])).toEqual([1,2]);
    expect(main.arrayFlatten([1, 2, 3])).toEqual([1, 2, 3]);
});

//======================================================================================
test('arrayDeepReverse', () => {
    expect(main.arrayDeepReverse([])).toEqual([]);
    expect(main.arrayDeepReverse([1, 2, 3])).toEqual([3,2,1]);
    expect(main.arrayDeepReverse([1, [1, 2], 3])).toEqual([3, [2, 1], 1]);
});

check.it('arrayDeepReverse-idempotent', [gen.nested(gen.array, gen.int)], (arr) => {
    expect(main.arrayDeepReverse(main.arrayDeepReverse(arr))).toEqual(arr);
}); 
