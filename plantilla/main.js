//=============================================
function arrayEqual(arr1, arr2) {
 if (arr1.length!=arr2.length)
	return false;
	for (var i=0; i<arr1.length;i++){
		if (arr1[i].toString()!= arr2[i].toString())
			return false;
	}
	return true;
}

//==================================================================
function lastIndexWithCompare(arr, v, comp = (x,y) => {x === y}) {
	if(Array.isArray(v)){
		for(var i=0; i<arr.length; i++){
			if(comp(arr[i],v)) return i;
		}
		return -1;
	}
	else return arr.lastIndexOf(v);
}

//=============================================
function arrayMember(arr, v) {
    if(Array.isArray(v)){
		for(let i=0; i<arr.length; i++){
			if(arrayEqual(arr[i],v)) return true;
		}
		return false;
	}else{
		if(lastIndexWithCompare(arr,v)>-1) return true;
		else return false;
	}
}

//=============================================
function arrayFilter(arr, p) {
	var arr2 =[];
	var j=0;
	for (var i=0; i< arr.length; i++){
		if (p(arr[i])!=0){
			arr2.push(arr[i]);
		}
	}
	return arr2;
}

//=============================================
function arrayMax(arr) {
	if (arr.length==0)
		return undefined;
	var mayor=0;
	for (var i=0; i<arr.length; i++){
		if (arr[i]> mayor)
			mayor = arr[i];
	}
	return mayor;
}


//=============================================
function arrayMin(arr) {
	if (arr.length==0)
		return null;
	var menor=0;
	for (var i=0; i<arr.length; i++){
		if (arr[i]<= menor)
			menor = arr[i];
	}
	return menor;
}		

//=============================================
function arrayReverse(arr) {
	var inverso = [];
	if (arr.length==0)
		return inverso;
	for (var i=arr.length-1; i>=0; i--){
		inverso.push(arr[i]);
	}
		return inverso;
}		

//=============================================
function arrayTake(arr, n) {
	var arr2 = [];
	if (arr.length==0)
		return arr2;
	for (var i=0; i<n; i++){
		arr2.push(arr[i]);
	}
	return arr2;	
}

//=============================================
function arrayDrop(arr, n) {
	var arr2 = [];
	if (arr.length==0)
		return arr2;
	for (var i=n; i<arr.length; i++){
		arr2.push(arr[i]);
	}
	return arr2;
}

//=============================================
function arrayRange(arr, a, b) {
	var arr2 = new Array();
	if (arr.length<2){
		return arr2;
	}
	for (var i=a; i<b; i++){
		if (i>arr.length-1)
		return arr2;
		arr2.push(arr[i]);
	}
	return arr2;
}

//=============================================
function arrayAppend(arr1, arr2) {
	var a = arr1.length;
	var b = arr2.length;
	var arr3 = [];
	for (i=0; i<(a+b);i++){
		if (i<a)
			arr3[i]=arr1[i];
		if (i>=a)
			arr3[i]=arr2[i-a];
	}
	return arr3;
}

//=============================================
function arrayMinus(arr) {
	var resta=arr[0];
	if (arr.length<1)
		return 0;
	for (var i=1; i<arr.length; i++){
		
		resta = resta-arr[i];
	}
	return resta;
}

//=============================================
function arrayAdd(arr) {
	var suma=arr[0];
	if (arr.length<1)
		return 0;
	for (var i=1; i<arr.length; i++){
		suma = suma+arr[i];
	}
	return suma;
}

//=============================================
function arrayMult(arr) {
	var mult=arr[0];
	if (arr.length<1)
		return 1;
	for (var i=1; i<arr.length; i++){
		mult = mult*arr[i];
	}
	return mult;
}

//=============================================
function arrayFold(f, arr, v) {
	var arr2=v;
	if (arr.length<1)
		return 1;
	for (var i=0; i<arr.length; i++){
		arr2 = f(arr2,arr[i]);
	}
	return arr2;
}

//=============================================
function arraySquare(arr) {
	var arr2 = [];
	if (arr.length==0)
		return arr;
	for (var i=0; i<arr.length; i++){
		arr2[i] = Math.pow(arr[i],2);
	}
	return arr2;
}

//=============================================
function arrayAddOne(arr) {
	if (arr.length<1)
		return arr;
	arr2=[];
	for (var i=0; i<arr.length; i++){
		arr2[i]=arr[i]+1;
	}
	return arr2;
}

//=============================================
function arrayMap(arr, f) {
	if (arr.length<1)
		return arr;
	arr2=[];
	for (var i=0; i<arr.length; i++){
		arr2[i]=f(arr[i]);
	}
	return arr2;
}

//=============================================
function arrayFlatten(arr) {
	var arr2 = new Array();
  	for(var i=0; i<arr.length; i++){
		if( Array.isArray(arr[i]) ){
			var arr3= new Array();
			arr3=arrayFlatten(arr[i]);
			for(var j=0; j<arr3.length; j++){
				arr2.push(arr3[j]);
			}
		}else arr2.push(arr[i]);
	}
	return arr2;
}

//===================================;==========
function arrayDeepReverse(arr) {
	var severla = [];
	for(var i=arr.length-1; i>=0; i--){
		if(Array.isArray(arr[i])){
			severla.push(arrayDeepReverse(arr[i]));
		}else severla.push(arr[i]);
	}
	return severla;
}

//****** MODULE EXPORTS */
exports.arrayEqual = arrayEqual;
exports.lastIndexWithCompare = lastIndexWithCompare;
exports.arrayMember = arrayMember;
exports.arrayFilter = arrayFilter;
exports.arrayMax = arrayMax;
exports.arrayMin = arrayMin;
exports.arrayReverse = arrayReverse;
exports.arrayTake = arrayTake
exports.arrayDrop = arrayDrop;
exports.arrayRange = arrayRange;
exports.arrayAppend = arrayAppend;
exports.arrayMinus = arrayMinus;
exports.arrayAdd = arrayAdd;
exports.arrayMult = arrayMult;
exports.arrayFold = arrayFold;
exports.arraySquare = arraySquare;
exports.arrayAddOne = arrayAddOne;
exports.arrayMap = arrayMap;
exports.arrayFlatten = arrayFlatten;
exports.arrayDeepReverse = arrayDeepReverse;