## Instrucciones

Debe completar el archivo `main.js` con la implementación de cada una de las funciones requeridas.

## Ejecutar tests

Para ejecutar los tests, primero debe instalar las dependencias ejecutando el comando:

```
npm install
```

Dentro de esta carpeta. Luego, para ejecutar los tests debe ingresar el comando:

```
npm test
```

El ejecutable `npm` viene disponible en la instalación de Node.js.